# Modern C++ Workshop

**Slides for a modern C++ workshop.**

## Licence

The code snippets are licenced under the GPLv3.0 licence.
The presentation is licenced under the Creative Commons 4.0 BY-NC-SA licence.
